<?php
// Projet: M152_MiniBlog
// Script: Vue displayToolBox.php
// Description: construit le code HTML pour l'affichage des différents médias
// Auteur: Briard Thibaud
// Version 1.0.0 PC 24.02.2021, version initial
// Version 1.1.0 PC 24.02.2021, ajout de l'affichage des vidéos
// Version 1.2.0 PC 03.03.2021, ajout de l'affichage de fichiers audios
// Version 1.2.1 PC 10.03.2021, modification de displayMedias pour afficher les audios séparément du carousel (vidéo/image)
// Version 1.3.0 PC 17.03.2021, ajout de l'affichage dees médias sous forme d'une liste de gestion



/**
 * récupère les enregistrements de la table media qui correspondent à l'id de post donnée
 * @param int $idPost : l'id du post dont il faut retourner les images
 * @return string code HTML pour l'affichage du/des média(s) sous forme de carousel/d'affichage
 */
function displayMedias($idPost) {
    $output = "";
    $audioFiles = [];

    // Si le post contient au un média
    if (getNumberOfMediaForPost($idPost) > 0) {
        $medias = getAllMediasFormPost($idPost);
        // Si le post contient plus d'un média
        if (getNumberOfImagesOrVideosForPost($idPost) > 1) {
            $output .= "<div id=\"carousel_post$idPost\" class=\"carousel slide\" data-interval=\"false\">";
            $output .= "<ol class=\"carousel-indicators\" style=\"padding-bottom:10px\">";
            foreach ($medias as $index => $media) {
                $main = ($index == 0) ? 'active' : '';
                if (explode("/", $media['typeMedia'])[0] != "audio") {
                    $output .= "<li data-target=\"#carousel_post$idPost\" data-slide-to=\"0\" class=\"$main\"></li>";
                }
            }
            $output .= "</ol>";
            $output .= "<div class=\"carousel-inner\">";
            foreach ($medias as $index => $media) {
                $main = ($index == 0) ? ' active' : '';
                // Empeche l'audio de se retrouver dans le carousel
                if (explode("/", $media['typeMedia'])[0] == "audio") {
                    array_push($audioFiles, $media);
                }
                else {
                    $output .= "<div class=\"carousel-item $main\">";
                    $output .= mediaTypeSelector($media);
                    $output .= "</div>";
                }
            }
            $output .= "</div>";
            $output .= "<a class=\"carousel-control-prev h-75\" href=\"#carousel_post$idPost\" role=\"button\" data-slide=\"prev\">";
            $output .= "<span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>";
            $output .= "<span class=\"sr-only\">Previous</span>";
            $output .= "</a>";
            $output .= "<a class=\"carousel-control-next h-75\" href=\"#carousel_post$idPost\" role=\"button\" data-slide=\"next\">";
            $output .= "<span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>";
            $output .= "<span class=\"sr-only\">Next</span>";
            $output .= "</a>";
            $output .= "</div>";
        }
        else {
            foreach ($medias as $index => $media) {
                if (explode("/", $media['typeMedia'])[0] == "audio") {
                    array_push($audioFiles, $media);
                }
                else {
                    $output .= mediaTypeSelector($media);
                }
            }
        }
    
        $medias = $audioFiles;
        foreach ($medias as $index => $media) {
            $output .= mediaTypeSelector($media);
        }
    }

    return $output;
}

/**
 * récupère les enregistrements de la table media qui correspondent à l'id de post donnée
 * @param int $idPost : l'id du post dont il faut retourner les images
 * @return string code HTML pour l'affichage du/des média(s) sous forme de petite liste de gestion
 */
function displayMediasList($idPost) {
    $output = "";
    $medias = getAllMediasFormPost($idPost);
    
    $output .= "<div class=\"form-check\">";
    foreach ($medias as $index => $media) {
        $output .= "<input class=\"form-check-input\" type=\"checkbox\" value=\"" . $media['idMedia'] . "\" id=\"old_media_" . $media['idMedia'] . "\" name=\"old_medias[]\">";
        $output .= "<div class=\"form-check-label media\" for=\"media_" . $media['idMedia'] . "\">";
        $output .= mediaTypeSelector($media, "mr-3", "width: 100px");
        $output .= "<div class=\"media-body\">";
        $output .= "<h5 class=\"mt-0\">" . $media['nameMedia'] . "</h5>";
        $output .= "<p>Type : " . $media['typeMedia'] . "</p>";
        $output .= "<p>Date : " . $media['creationDate'] . "</p>";
        $output .= "</div>";
        $output .= "</div>";
        $output .= "<hr class=\"my-4\">";
    }
    $output .= "</div>";

    return $output;
}

function mediaTypeSelector($media, $class = "card-img-top w-100", $style = "") {
    $output = "";
    
    switch (explode("/", $media['typeMedia'])[0]) {
        case 'image':
            $output = displayImage($media, $class, $style);
            break;

        case 'video':
            $output = displayVideo($media, $class, $style);
            break;
        
        case 'audio':
            $output = displayAudio($media, $class, $style);
            break;
            
        default:
            # code...
            break;
    }

    return $output;
}

function displayImage($media, $class, $style) {
    $output = "";

    $output .= "<img class=\"" . $class . "\" style=\"" . $style . "\" src=\"medias/" . $media['nameMedia'] . "\" alt=\"" . $media['nameMedia'] . "\">";

    return $output;
}

function displayVideo($media, $class, $style) {
    $output = "";

    $output .= "<video class=\"" . $class . "\" style=\"" . $style . "\" controls autoplay muted loop>";
    $output .= "<source src=\"medias/" . $media['nameMedia'] . "\" type=\"" . $media['typeMedia'] . "\">";
    $output .= "</video>";

    return $output;
}

function displayAudio($media, $class, $style) {
    $output = "";

    $output .= "<audio  class=\"" . $class . "\" style=\"" . $style . "\" controls>";
    $output .= "<source src=\"medias/" . $media['nameMedia'] . "\" type=\"" . $media['typeMedia'] . "\">";
    $output .= "</audio>";

    return $output;
}