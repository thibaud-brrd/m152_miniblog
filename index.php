<?php
// Projet: M152_MiniBlog
// Script: Contrôleur index.php
// Description: affiche la page d'acceuil du site
// Auteur: Briard Thibaud
// Version 1.0.0 PC 27.01.2021, version initial

include 'views/home.php';