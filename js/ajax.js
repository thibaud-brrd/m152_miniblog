// Projet: M152_MiniBlog
// Script: Modèle/AJAX ajax.js
// Description: Réalise des requêtes http pour envoyer et recevoir les données
// Auteur: Briard Thibaud
// Version 1.0.0 PC 24.03.2021, version initial

const ALERT = document.getElementById('form-alert');
const ALERT_TITLE = document.getElementById('form-alert-title');
const ALERT_CONTENT = document.getElementById('form-alert-content');
const PROGRESS = document.getElementById('upload-progress');
const PROGRESS_BAR = document.getElementById('upload-progress-bar');

function cancelPost() {
    window.location = ".";
}

function createPost() {
    const URL = "./postFilter.php?submit=create";
    const FORM = document.getElementById('form-post-create');

    let formData = new FormData(FORM);
    let xhttp = new XMLHttpRequest();

    PROGRESS.style.display = "none";
    PROGRESS_BAR.classList.remove("bg-danger");
    PROGRESS_BAR.classList.add("bg-primary");
    PROGRESS_BAR.classList.add("progress-bar-striped");
    PROGRESS_BAR.classList.add("progress-bar-animated");
    PROGRESS_BAR.innerHTML = '';

    // Upload progress
    xhttp.upload.addEventListener("progress", function(evt){
        if (evt.lengthComputable) {
            let percentComplete = Math.round(evt.loaded / (evt.total / 100));
            PROGRESS.style.display = "block";
            PROGRESS_BAR.style.width = percentComplete + "%";
            PROGRESS_BAR.setAttribute("aria-valuenow", percentComplete);
            if (percentComplete == 100) {
            }
        }
    }, false);
    // Response
    xhttp.onreadystatechange = function() {
        if (this.readyState == XMLHttpRequest.DONE) {
            error = JSON.parse(this.responseText);

            if (this.status == 400) {
                PROGRESS_BAR.classList.remove("progress-bar-striped");
                PROGRESS_BAR.classList.remove("progress-bar-animated");
                PROGRESS_BAR.classList.remove("bg-primary");
                PROGRESS_BAR.classList.add("bg-danger");
                PROGRESS_BAR.innerHTML = '<span aria-hidden="true"><img src="ressources/icons/x.svg" alt="error"></span>';

                ALERT.classList.remove("alert-success");
                ALERT.classList.add("alert-danger");
                ALERT_TITLE.innerHTML = "Erreur dans la création du Post !";
                ALERT_CONTENT.innerHTML = error['message'];
                ALERT.classList.add("show");
            }
            else if (this.status == 201) {
                PROGRESS_BAR.classList.remove("progress-bar-striped");
                PROGRESS_BAR.classList.remove("progress-bar-animated");
                PROGRESS_BAR.classList.remove("bg-primary");
                PROGRESS_BAR.classList.add("bg-success");
                PROGRESS_BAR.innerHTML = '<span aria-hidden="true"><img src="ressources/icons/check.svg" alt="error"></span>';

                ALERT.classList.remove("alert-danger");
                ALERT.classList.add("alert-success");
                ALERT_TITLE.innerHTML = "Post créé avec succès (redirection)";
                ALERT_CONTENT.innerHTML = error['message'];
                ALERT.classList.add("show");
                window.location = ".";
            }
            else {
                console.error(this.status + " when call " + URL);
                alert(this.status);
            }
        }
    };

    xhttp.open("POST", URL, true);
    xhttp.send(formData);
}

function updatePost(idPost) {
    const URL = "./postFilter.php?submit=update&idPost=" + idPost;
    const FORM = document.getElementById('form-post-update');
    
    let formData = new FormData(FORM);
    let xhttp = new XMLHttpRequest();
    
    PROGRESS.style.display = "none";
    PROGRESS_BAR.classList.remove("bg-danger");
    PROGRESS_BAR.classList.add("bg-primary");
    PROGRESS_BAR.classList.add("progress-bar-striped");
    PROGRESS_BAR.classList.add("progress-bar-animated");
    PROGRESS_BAR.innerHTML = '';
    
    // Upload progress
    xhttp.upload.addEventListener("progress", function(evt){
        if (evt.lengthComputable) {
            let percentComplete = Math.round(evt.loaded / (evt.total / 100));
            PROGRESS.style.display = "block";
            PROGRESS_BAR.style.width = percentComplete + "%";
            PROGRESS_BAR.setAttribute("aria-valuenow", percentComplete);
            if (percentComplete == 100) {
            }
        }
    }, false);
    // Response
    xhttp.onreadystatechange = function() {
        if (this.readyState == XMLHttpRequest.DONE) {
            error = JSON.parse(this.responseText);

            if (this.status == 400) {
                PROGRESS_BAR.classList.remove("progress-bar-striped");
                PROGRESS_BAR.classList.remove("progress-bar-animated");
                PROGRESS_BAR.classList.remove("bg-primary");
                PROGRESS_BAR.classList.add("bg-danger");
                PROGRESS_BAR.innerHTML = '<span aria-hidden="true"><img src="ressources/icons/x.svg" alt="error"></span>';

                ALERT.classList.remove("alert-success");
                ALERT.classList.add("alert-danger");
                ALERT_TITLE.innerHTML = "Erreur dans la modification du Post !";
                ALERT_CONTENT.innerHTML = error['message'];
                ALERT.classList.add("show");
            }
            else if (this.status == 200) {
                PROGRESS_BAR.classList.remove("progress-bar-striped");
                PROGRESS_BAR.classList.remove("progress-bar-animated");
                PROGRESS_BAR.classList.remove("bg-primary");
                PROGRESS_BAR.classList.add("bg-success");
                PROGRESS_BAR.innerHTML = '<span aria-hidden="true"><img src="ressources/icons/check.svg" alt="error"></span>';

                ALERT.classList.remove("alert-danger");
                ALERT.classList.add("alert-success");
                ALERT_TITLE.innerHTML = "Post modifié avec succès (redirection)";
                ALERT_CONTENT.innerHTML = error['message'];
                ALERT.classList.add("show");
                window.location = ".";
            }
            else {
                console.error(this.status + " when call " + URL);
                alert(this.status);
            }
        }
    };

    xhttp.open("POST", URL, true);
    xhttp.send(formData);
}

function deletePost(idPost) {
    const URL = "./postFilter.php?submit=delete&idPost=" + idPost;
    const FORM = document.getElementById('form-post-create');

    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == XMLHttpRequest.DONE) {
            error = JSON.parse(this.responseText);
            console.log(error);

            if (this.status == 400) {
                ALERT.classList.remove("alert-success");
                ALERT.classList.add("alert-danger");
                ALERT_TITLE.innerHTML = "Erreur dans la suppréssion du post !";
                ALERT_CONTENT.innerHTML = error['message'];
                ALERT.classList.add("show");
            }
            else if (this.status == 200) {
                console.log("status 204")
                ALERT.classList.remove("alert-danger");
                ALERT.classList.add("alert-success");
                ALERT_TITLE.innerHTML = "Post supprimé avec succès (redirection)";
                ALERT_CONTENT.innerHTML = error['message'];
                ALERT.classList.add("show");
                window.location = ".";
            }
            else {
                console.error(this.status + " when call " + URL);
                alert(this.status);
            }
        }
    };

    xhttp.open("DELETE", URL, true);
    xhttp.send();
}