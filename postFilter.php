<?php
// Projet: M152_MiniBlog
// Script: Contrôleur modifyPostFilter.php
// Description: verifie le contenu du post et redirige vers la bonne page
// Auteur: Briard Thibaud
// Version 1.0.0 PC 17.03.2021, version initial
// Version 1.1.0 PC 18.03.2021, ajout du filtrage pour la suppresion
// Version 2.0.0 PC 22.03.2021, fusion avec la page filtration de création de post
//   Information version page fusionnée :
//     --> Version 1.0.0 PC 27.01.2021, version initial
//     --> Version 1.1.0 PC 10.02.2021, ajout du "check type" / ajout des limitations pour la DB / ajout des fonctionnalité d'envoi à la DB
//     --> Version 1.1.1 PC 11.02.2021, correctif en cas d'envoi de post sans images
//     --> Version 1.2.0 PC 24.02.2021, ajout des fichier de type vidéo à la liste des fichiers accepter
//     --> Version 1.3.0 PC 03.03.2021, ajout des fichier de type audio à la liste des fichiers accepter
//     --> Version 1.4.0 PC 03.03.2021, supression des images dans le dossier si la transaction n'est pas concluante

/* Includes :
**   --> models/post.php : page des fonctions pour la table post de la base de donnée
**   --> models/media.php : page des fonctions pour la table media de la base de donnée
**   --> filterToolbox.php : page des fonctions pour la vérification des inputs du formulaire
*/
include "models/post.php";
include "models/media.php";
include "filterToolbox.php";

// Initialisation variables d'erreur
$error = [
    'flag' => false,
    'message' => ""
];

// Vérifie que la page a bien été accédé par un formulaire
if (filter_has_var(INPUT_GET,'submit')) {
    // Récupération de l'action du submit et la date
    $submit = filter_input(INPUT_GET, 'submit', FILTER_DEFAULT);
    $idPost = null;
    $postDate = date("Y-m-d_H-i-s");

    // Début de la transaction
    connectDB()->beginTransaction();
    try {
        switch ($submit) {
            case 'create':
                // Récupération des informations du formulaire
                $comment = filter_input(INPUT_POST, 'comment', FILTER_SANITIZE_STRING);

                $pageTitle = "Create new Post";

                // En-tête du ticket d'erreur
                $error['message'] .= "action : creer\n";

                
                // Ajout du commentaire
                $error['message'] .= "comment : ";
                // Validation du commentaire
                ValidateCommentLength($comment);

                // Save comment
                $idPost = createPost($comment);
                $error['message'] .= "Valide\n";
                
                // Ajout des medias
                $error['message'] .= "new_medias : \n";
                // Vérifie si il y a des fichiers, sinon passe a la suite
                if ($_FILES['new_medias']['error'][0] != UPLOAD_ERR_NO_FILE) {
                    // Vérifie que l'input des médias (quand bien même il soit vide) existe bien
                    if (isset($_FILES['new_medias']) || $_FILES['new_medias']['error'][0] != UPLOAD_ERR_NO_TMP_DIR) {
                        for ($i=0; $i < count($_FILES["new_medias"]["name"]); $i++) {
                            $media = [];
                            // Récupère les information du fichier numéro [i]
                            $media['name'] = explode(".", $_FILES['new_medias']['name'][$i])[0];
                            $media['ext'] = "." . explode(".", $_FILES['new_medias']['name'][$i])[1];
                            $media['tmp_name'] = $_FILES['new_medias']['tmp_name'][$i];
                            $media['size'] = $_FILES['new_medias']['size'][$i];
                            $media['type'] = $_FILES['new_medias']['type'][$i];
                            $media['error'] = $_FILES['new_medias']['error'][$i];
                            // génération d'un nom unique
                            $media['path'] = $postDate . "_" . $i . "_" . $media['name'];
                            // coupe le nom du fichier pour faire 100 caractères maximum
                            $media['path'] = substr($media['path'], 0, 100 - strlen($media['ext'])) . $media['ext'];
                            
                            $upFile = "medias/" . $media['path'];

                            // Validation du média
                            $error['message'] .= $media['name'] . $media['ext'] . " : ";
                            ValidateFileErrorCode($media['error']);
                            ValidateFileType($media['type']);
                            ValidateFileSize($media['size'], $media['type']);
                            ValidateFileUpload($media['tmp_name']);
                            ValidateFileMove($media['tmp_name'], $upFile);

                            // Tout est en ordre pour ce média
                            createMedia($media['path'], $media['type'], $idPost);
                            $error['message'] .= "Valide\n";
                        }
                    }
                    else {
                        throw new Exception("les médias n'ont pas pu être récupèrer");
                    }
                }
                else {
                    $error['message'] .= "aucun médias à ajouter\n";
                }

                break;
            
            case 'update':
                // Récupération des informations du formulaire
                $idPost = filter_input(INPUT_GET,'idPost', FILTER_VALIDATE_INT);
                $comment = filter_input(INPUT_POST, 'comment', FILTER_SANITIZE_STRING);
                $old_medias = filter_input(INPUT_POST, 'old_medias', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
                $post = getPost($idPost);

                $pageTitle = "Edit Post [" . $idPost . "]";

                // En-tête du ticket d'erreur
                $error['message'] .= "action : editer\n";
                $error['message'] .= "id post : [" . $idPost . "]\n";
                $error['message'] .= "ids delete medias :\n";
                if (!is_null($old_medias) || is_array($old_medias)) {
                    foreach ($old_medias as $key => $id) {
                        $error['message'] .= "\t" . $key . " - [" . $id . "]\n";
                    }
                }

                // Modification du Post
                ValidatePostExist($idPost);
                // Modification du commentaire
                $error['message'] .= "comment : ";
                if (CheckCommentChange($comment, getPost($idPost)['comment'])) {
                    // Validation du commentaire
                    ValidateCommentLength($comment);

                    // Save comment
                    updatePostComment($idPost, $comment);
                    $error['message'] .= "comment : Valide\n";
                }

                // Ajout des nouveaux medias
                $error['message'] .= "new_medias : \n";
                // Vérifie si il y a des fichiers, sinon passe a la suite
                if ($_FILES['new_medias']['error'][0] != UPLOAD_ERR_NO_FILE) {
                    // Vérifie que l'input des médias (quand bien même il soit vide) existe bien
                    if (isset($_FILES['new_medias']) || $_FILES['new_medias']['error'][0] != UPLOAD_ERR_NO_TMP_DIR) {
                        for ($i=0; $i < count($_FILES["new_medias"]["name"]); $i++) {
                            $media = [];
                            // Récupère les information du fichier numéro [i]
                            $media['name'] = explode(".", $_FILES['new_medias']['name'][$i])[0];
                            $media['ext'] = "." . explode(".", $_FILES['new_medias']['name'][$i])[1];
                            $media['tmp_name'] = $_FILES['new_medias']['tmp_name'][$i];
                            $media['size'] = $_FILES['new_medias']['size'][$i];
                            $media['type'] = $_FILES['new_medias']['type'][$i];
                            $media['error'] = $_FILES['new_medias']['error'][$i];
                            // génération d'un nom unique
                            $media['path'] = $postDate . "_" . $i . "_" . $media['name'];
                            // coupe le nom du fichier pour faire 100 caractères maximum
                            $media['path'] = substr($media['path'], 0, 100 - strlen($media['ext'])) . $media['ext'];
                            
                            $upFile = "medias/" . $media['path'];

                            // Validation du média
                            $error['message'] .= $media['name'] . $media['ext'] . " : ";
                            ValidateFileErrorCode($media['error']);
                            ValidateFileType($media['type']);
                            ValidateFileSize($media['size'], $media['type']);
                            ValidateFileUpload($media['tmp_name']);
                            ValidateFileMove($media['tmp_name'], $upFile);

                            // Tout est en ordre pour ce média
                            createMedia($media['path'], $media['type'], $idPost);
                            $error['message'] .= "Valide\n";
                        }
                    }
                    else {
                        throw new Exception("les médias n'ont pas pu être récupèrer");
                    }
                }
                else {
                    $error['message'] .= "aucun médias à ajouter\n";
                }

                if ($old_medias) {
                    foreach ($old_medias as $key => $idMedia) {
                        ValidateMediaExist($idMedia);
                        $media = getMedia($idMedia);
                        deleteMedia($idMedia);
                        $upfile = "medias/" . $media['nameMedia'];
                        if(is_file($upfile)) {
                            unlink($upfile);
                            $error['message'] .= $media['nameMedia'] . " : suppréssion du média\n";
                        }
                    }
                }
                
                break;

            case 'delete':
                // Récupération des informations du formulaire
                $idPost = filter_input(INPUT_GET,'idPost', FILTER_VALIDATE_INT);

                $pageTitle = "Delete Post [" . $idPost . "]";

                $error['message'] .= "action : supprimer\n";
                $error['message'] .= "id post : [" . $idPost . "]\n";
                
                // Suppression du post
                ValidatePostExist($idPost);
                deletePost($idPost);
                $error['message'] .= "le post " . $idPost . " a été supprimer\n";

                // Suppression des médias
                $medias = getAllMediasFormPost($idPost);
                foreach ($medias as $key => $media) {
                    if (file_exists("medias/" . $media['nameMedia'])) {
                        unlink("medias/" . $media['nameMedia']);
                        $error['message'] .= "le média " . $media['nameMedia'] . " a été supprimé\n";
                    }
                    else {
                        $error['message'] .= "le média " . $media['nameMedia'] . " n'a pas été trouvé\n";
                    }
                }
                deleteAllMediasFromPost($idPost);

                break;
            default:
                $error['message'] .= "action : ERREUR --> une mauvaise action a été envoyé\n";
                $error['flag'] = true;
                break;
        }
    }
    catch (Exception $e) {
        $error['message'] .= "Une exception est survenue\n";
        $error['message'] .= "\texception >>> " . $e->getMessage() . "\n";
        $error['flag'] = true;
    }

    if ($error['flag']) {
        // Annule la transaction
        connectDB()->rollBack();
        if ($submit == 'create' || $submit == 'update') {
            // Supprimer les fichiers si il y en a
            // Vérifie si il y a des fichiers, sinon passe a la suite
            if ($_FILES['new_medias']['error'][0] != UPLOAD_ERR_NO_FILE) {
                // Vérifie que l'input des médias (quand bien même il soit vide) existe bien
                if (isset($_FILES['new_medias']) || $_FILES['new_medias']['error'][0] != UPLOAD_ERR_NO_TMP_DIR) {
                    for ($i=0; $i < count($_FILES["new_medias"]["name"]); $i++) {
                        $media = [];
                        // Récupère les information du fichier numéro [i]
                        $media['name'] = explode(".", $_FILES['new_medias']['name'][$i])[0];
                        $media['ext'] = "." . explode(".", $_FILES['new_medias']['name'][$i])[1];
                        // génération d'un nom unique
                        $media['path'] = $postDate . "_" . $i . "_" . $media['name'];
                        // coupe le nom du fichier pour faire 100 caractères maximum
                        $media['path'] = substr($media['path'], 0, 100 - strlen($media['ext'])) . $media['ext'];

                        $upfile = "medias/" . $media['path'];
            
                        if(is_file($upfile)) {
                            unlink($upfile);
                            $error['message'] .= $media['name'] . " : suppréssion du média\n";
                        }
                    }
                }
            }
        }

        http_response_code(400);
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json;charset=utf-8');
        echo(json_encode($error));
    }
    else {
        // Valide la transaction
        connectDB()->commit();
        // Change error message
        $error['message'] = "Aucune erreur détecté.\nLe post est enregistrer.\nVous allez être rediriger sur la page principal.";
    
        // Message de validation et renvoi à la page principal

        if ($submit == 'create') {
            http_response_code(201);
        }
        else {
            http_response_code(200);
        }
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json;charset=utf-8');
        echo(json_encode($error));
    }
}
