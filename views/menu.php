<?php
// Projet: M152_MiniBlog
// Script: Vue menu.php
// Description: menu dynamique du site
// Auteur: Briard Thibaud
// Version 1.0.0 PC 27.01.2021, version initial

?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Blog Destiny 2</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item">
        <a class="nav-link" href="./">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./post.php?submit=create">Post</a> <!-- role="button" data-toggle="modal" data-target="#postModal" -->
      </li>
    </ul>
  </div>
</nav>