<?php
// Projet: M152_MiniBlog
// Script: Contrôleur filterToolbox.php
// Description: les fonctions de filtrages des posts
// Auteur: Briard Thibaud
// Version 1.0.0 PC 03.03.2021, version initial
// Version 1.1.0 PC 22.03.2021, ajout de la validation du commentaire
// Version 1.1.0 PC 24.03.2021, modification des méthodes pour l'utilisation de "throws"

// Constantes
const COMMENT_MAX_LENGTH = 1000;

const ACCEPTED_IMAGES_TYPES = ["image/png", "image/jpg", "image/jpeg"];
const ACCEPTED_VIDEOS_TYPES = ["video/mp4", "video/webm", "video/ogg"];
const ACCEPTED_AUDIOS_TYPES = ["audio/mp3", "audio/mpeg", "audio/wav"];

const MAX_IMAGES_FILESIZE = 3000000;  //  3Mo
const MAX_VIDEOS_FILESIZE = 50000000; // 50Mo
const MAX_AUDIOS_FILESIZE = 10000000; // 10Mo

// Functions

/**
 * vérifie si la taille du commentaire est en dessous (ou égal) à la taille limite défini
 * @param int idPost : l'id du post a vérifier
 * @return bool true si il est valider, false si ça n'est pas le cas
 */
function ValidatePostExist($idPost) {
    if (!checkPost($idPost)) {
        throw new Exception("le post [" . $idPost . "] n'existe pas dans la base de donnée");
    }
    return false;
}

/**
 * vérifie si la taille du commentaire est en dessous (ou égal) à la taille limite défini
 * @param int idPost : l'id du post a vérifier
 * @return bool true si il est valider, false si ça n'est pas le cas
 */
function ValidateMediaExist($idMedia) {
    if (!checkMedia($idMedia)) {
        throw new Exception("le média [" . $idMedia . "] n'existe pas dans la base de donnée");
    }
    return false;
}

/**
 * vérifie si la taille du commentaire est en dessous (ou égal) à la taille limite défini
 * @param string comment : le commentaire à vérifier
 * @return bool true si il est valider, false si ça n'est pas le cas
 */
function ValidateCommentLength($comment) {
    if (strlen($comment) > COMMENT_MAX_LENGTH) {
        throw new Exception("votre commentaire est trop grand, il fait " . strlen($comment) . " caractères, il peu faire maximum " . COMMENT_MAX_LENGTH . " caractères");
    }
    return false;
}

/**
 * vérifie si le fichier n'a pas de code d'erreur autre que UPLOAD_ERR_OK (0)
 * @param string comment : le nouveau commentaire
 * @param string comment : l'ancien commentaire
 * @return bool true si il est valider, false si ça n'est pas le cas
 */
function CheckCommentChange($newComment, $oldComment) {
    if ($newComment != $oldComment) {
        return true;
    }
    return false;
}

/**
 * vérifie si le fichier n'a pas de code d'erreur autre que UPLOAD_ERR_OK (0)
 * @param int fileErrorCode : le code d'erreur du fichier
 * @return bool true si il est valider, false si ça n'est pas le cas
 */
function ValidateFileErrorCode($fileErrorCode) {
    switch ($fileErrorCode) {
        case UPLOAD_ERR_OK:
            return true;
            break;

        case UPLOAD_ERR_INI_SIZE:
            throw new Exception("la taille du fichier excède la taille max de la directive upload_max_filesize défini dans php.ini");
            break;

        case UPLOAD_ERR_FORM_SIZE:
            throw new Exception("la taille du fichier excède la taille max de la directive MAX_FILE_SIZE défini par le formulaire html");
            break;
            
        case UPLOAD_ERR_PARTIAL:
            throw new Exception("le fichier n'a été que partiellement télécharger");
            break;
            
        case UPLOAD_ERR_CANT_WRITE:
            throw new Exception("il a été impossible d'écrire le fichier sur le serveur");
            break;
            
        case UPLOAD_ERR_EXTENSION:
            throw new Exception("une extension a stoppé le téléchargement du fichier");
            break;
        
        default:
            throw new Exception("une erreur inconnue est survenu lors de la validation du code d'erreur");
            break;
    }
}

/**
 * vérifie si le fichier correspond à un des types de fichiers acceptés
 * @param string fileType : le type du fichier
 * @return bool true si il est valider, false si ça n'est pas le cas
 */
function ValidateFileType($fileType) {
    if (IsImage($fileType)) {
        if (!in_array($fileType, ACCEPTED_IMAGES_TYPES)) {
            throw new Exception("ce type d'image [" . $fileType . "] n'est pas acceptés, seul les images suivantes sont acceptés : " . implode(", ", ACCEPTED_IMAGES_TYPES));
        }
    }
    else if (IsVideo($fileType)) {
        if (!in_array($fileType, ACCEPTED_VIDEOS_TYPES)) {
            throw new Exception("ce type de vidéo [" . $fileType . "] n'est pas acceptés, seul les vidèos suivantes sont acceptés : " . implode(", ", ACCEPTED_VIDEOS_TYPES));
        }
    }
    else if (IsAudio($fileType)) {
        if (!in_array($fileType, ACCEPTED_AUDIOS_TYPES)) {
            throw new Exception("ce type d'audio [" . $fileType . "] n'est pas acceptés, seul les audios suivants sont acceptés : " . implode(", ", ACCEPTED_AUDIOS_TYPES));
        }
    }
    else {
        throw new Exception("ce type de média [" . $fileType . "] n'est pas acceptés, seul les médias suivants sont acceptés : " . implode(", ", array_merge(ACCEPTED_IMAGES_TYPES, ACCEPTED_VIDEOS_TYPES, ACCEPTED_AUDIOS_TYPES)));
    }

    return true;
}

/**
 * vérifie si le fichier n'excéde pas les tailles défini pour chaque type de fichiers
 * @param int fileSize : la taille du fichier en bit
 * @param string fileType : le type du fichier
 * @return bool true si il est valider, false si ça n'est pas le cas
 */
function ValidateFileSize($fileSize, $fileType) {
    if (IsImage($fileType)) {
        if ($fileSize > MAX_IMAGES_FILESIZE) {
            throw new Exception("la taille de cette image [" . $fileSize / 1000000 . "Mo] est trop grandes, la taille des images ne doit pas dépasser : " . (MAX_IMAGES_FILESIZE / 1000000) . "Mo");
        }
    }
    else if (IsVideo($fileType)) {
        if ($fileSize > MAX_VIDEOS_FILESIZE) {
            throw new Exception("la taille de cette vidéo [" . $fileSize / 1000000 . "Mo] est trop grandes, la taille des vidéos ne doit pas dépasser : " . (MAX_VIDEOS_FILESIZE / 1000000) . "Mo");
        }
    }
    else if (IsAudio($fileType)) {
        if ($fileSize > MAX_AUDIOS_FILESIZE) {
            throw new Exception("la taille de cette audio [" . $fileSize / 1000000 . "Mo] est trop grandes, la taille des audios ne doit pas dépasser : " . (MAX_AUDIOS_FILESIZE / 1000000) . "Mo");
        }
    }
    else {
        throw new Exception("ce type de média [" . $fileType . "] n'est pas acceptés, seul les médias suivants sont acceptés : " . implode(", ", array_merge(ACCEPTED_IMAGES_TYPES, ACCEPTED_VIDEOS_TYPES, ACCEPTED_AUDIOS_TYPES)));
    }

    return true;
}

/**
 * vérifie si le fichier n'excéde pas les tailles défini pour chaque type de fichiers
 * @param string fileTemporaryName : le nom temporaire du fichier
 * @return bool true si il est valider, false si ça n'est pas le cas
 */
function ValidateFileUpload($fileTemporaryName) {
    if(!is_uploaded_file($fileTemporaryName)) {
        throw new Exception("une erreur est survenu lors de la validation de l'upload du fichier");
    }

    return true;
}

/**
 * vérifie si le fichier n'excéde pas les tailles défini pour chaque type de fichiers
 * @param string fileTemporaryName : le nom temporaire du fichier
 * @param string filePath : le chemin du dossier de destination
 * @return bool true si il est valider, false si ça n'est pas le cas
 */
function ValidateFileMove($fileTemporaryName, $filePath) {
    if(!move_uploaded_file($fileTemporaryName, $filePath)) {
        throw new Exception("une erreur est survenu lors de la validation de du transfert du fichier vers le dossier final");
    }

    return true;
}

function IsImage($fileType) {
    return substr($fileType, 0, 6) == 'image/';
}

function IsVideo($fileType) {
    return substr($fileType, 0, 6) == 'video/';
}

function IsAudio($fileType) {
    return substr($fileType, 0, 6) == 'audio/';
}