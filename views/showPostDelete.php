<?php
// Projet: M152_MiniBlog
// Script: Vue showPostDelete.php
// Description: Formulaire de suppréssion des posts (delete)
// Auteur: Briard Thibaud
// Version 1.0.0 PC 22.03.2021, version initial
?>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <form id="form-post-delete">
                    <div class="form-group">
                        <h5 class="card-title">Etes-vous sûr de vouloir supprimer le post N°<?= $idPost ?> ?</h5>
                        <p class="card-text">commentaire : <?= $post['comment'] ?></p>
                        <p class="card-text">Nombres de medias : <?= getNumberOfMediaForPost($idPost) ?></p>
                        <p class="card-text"><small class="text-muted">
                            <span><img src="ressources/icons/alert.svg" alt="warning"></span>&nbsp;ATTENTION : l'action est irréversible !
                        </small></p>
                    </div>
                </form>
                <div class="form-group">
                    <button class="btn btn-secondary" onclick="cancelPost()">Annuler</button>
                    <button class="btn btn-primary" onclick="deletePost(<?= $idPost ?>)">Valider</button>
                </div>
            </div>
        </div>
    </div>
</div>