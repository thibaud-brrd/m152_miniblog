<?php 
// Projet: M152_MiniBlog
// Script: Vue showPost.php
// Description: Page de création ou de modification des posts (update/delete)
// Auteur: Briard Thibaud
// Version 1.0.0 PC 17.03.2021, version initial
// Version 2.0.0 PC 22.03.2021, fusion avec la page de création de post
//   Information version page fusionnée :
//     --> Version 1.0.0 PC 27.01.2021, version initial
//     --> Version 1.1.0 PC 24.02.2021, ajout de la vidéo pour les types de fichiers accepter par l'input file
//     --> Version 1.2.0 PC 03.03.2021, ajout de l'audio pour les types de fichiers accepter par l'input file
// Version 2.1.0 PC 23.03.2021, séparation des formulaires dans des fichier propres

$pageTitle = "Post";

// Création d'un formulaire adéquat
if (filter_has_var(INPUT_GET,'submit')) {
    // récupération de l'action
    $submit = filter_input(INPUT_GET, 'submit', FILTER_SANITIZE_STRING);
}

include "models/post.php";
include "models/media.php";
include "displayToolbox.php";

include "header.php";
?>
<div class="alert alert-danger alert-dismissible collapse" role="alert" id="form-alert">
    <h4 id="form-alert-title" class="alert-heading"></h4>
    <button type="button" class="close" data-toggle="collapse" data-target="#form-alert" aria-label="Close" aria-expanded="false" aria-controls="form-alert">
        <span aria-hidden="true"><img src="ressources/icons/x.svg" alt="close"></span>
    </button>
    <hr>
    <pre id="form-alert-content"></pre>
</div>
<?php
    if ($submit == "create") {
        include "views/showPostCreate.php";
    }
    else {
        $idPost = filter_input(INPUT_GET, 'idPost', FILTER_VALIDATE_INT);
        $post = getPost($idPost);

        if ($submit == "update") {
            include "views/showPostUpdate.php";
        }
        else if ($submit == "delete") {
            include "views/showPostDelete.php";
        }
    }

include "footer.php"
?>
